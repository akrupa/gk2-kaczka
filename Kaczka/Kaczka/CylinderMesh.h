//
//  CylinderMesh.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 07/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__CylinderMesh__
#define __Robot__CylinderMesh__

#include <iostream>
#include "Mesh.h"

class CylinderMesh : public Mesh {
    
public:
    CylinderMesh();
};

#endif /* defined(__Robot__CylinderMesh__) */
