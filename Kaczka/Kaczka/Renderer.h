//
//  Renderer.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 23/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__Renderer__
#define __Robot__Renderer__

#include <iostream>
#include <vector>
#include <OpenGL/gl3.h>

#include "RobotShader.h"
#include "CubeShader.h"
#include "DuckShader.h"
#include "ArmMeshBuffer.h"
#include "Model.h"
#include "CameraController.h"
#include "glm/glm.hpp"
#include "WaterSimulator.h"

namespace Renderer {
    class Renderer {
    
        Shader::RobotShader *robotShader;
        Shader::CubeShader *cubeShader;
        Shader::DuckShader *duckShader;
        
        Buffer::ArmMeshBuffer *m_mirrorBuffer;
        Buffer::ArmMeshBuffer *m_cubeBuffer;
        Buffer::ArmMeshBuffer *m_duckBuffer;
        

        Model::Model *m_mirrorModel;
        Model::Model *m_cubeModel;
        Model::Model *m_duckModel;
        
        Simulator::WaterSimulator *m_waterSimulator;
        
        glm::vec3 m_lightPosition;
        
        
        void drawMirror(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 cameraPosition);
        void drawCube(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 cameraPosition);
        void drawDuck(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 cameraPosition);
    public:
        void setup();
        
        void setMirrorModel(Model::Model *mirrorModel);
        void setCubeModel(Model::Model *cubeModel);
        void setDuckModel(Model::Model *duckModel);
        
        void renderWithCameraController(Controller::CameraController *controller);
    };
}

#endif /* defined(__Robot__Renderer__) */
