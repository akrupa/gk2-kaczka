//
//  CylinderMesh.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 07/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "CylinderMesh.h"


CylinderMesh::CylinderMesh() : Mesh()
{
    float l = 3.0f;
    float r = 0.25f;
    
    const int Divs = 20;
    
    Vertex v;
    v.normal = glm::vec3(0.0f, 0.0f, 1.0f);
    v.position = glm::vec3(0.0f, 0.0f, l/2);
    vertexes.push_back(v);
    
    for (int i = 0; i < Divs; i++) {
        float angle = (float)i/Divs * 2.0 * M_PI;
        
        v.position = glm::vec3(r * cosf(angle), r * sinf(angle), l/2);
        vertexes.push_back(v);
    }
    
    for (int i = 0; i < Divs; i++) {
        Triangle t;
        t.indices.a = 0;
        t.indices.b = 1 + i;
        t.indices.c = 1 + (i + 1) % Divs;
        
        triangles.push_back(t);
    }
    
    
    v.normal = glm::vec3(0.0f, 0.0f, -1.0f);
    v.position = glm::vec3(0.0f, 0.0f, -l/2);
    vertexes.push_back(v);
    
    for (int i = 0; i < Divs; i++) {
        float angle = (float)i/Divs * 2.0 * M_PI;
        
        v.position = glm::vec3(r * cosf(angle), r * sinf(angle), -l/2);
        vertexes.push_back(v);
    }
    
    for (int i = 0; i < Divs; i++) {
        Triangle t;
        t.indices.a = Divs + 1;
        t.indices.b = Divs + 2 + (i + 1) % Divs;
        t.indices.c = Divs + 2 + i;
        
        triangles.push_back(t);
    }
    
    // side
    
    
    
    
    for (int i = 0; i < Divs; i++) {
        float angle = (float)i/Divs * 2.0 * M_PI;
        
        v.normal = glm::vec3(cosf(angle), sinf(angle), 0.0);
        
        v.position = glm::vec3(r * cosf(angle), r * sinf(angle),  l/2);
        vertexes.push_back(v);
        
        v.position = glm::vec3(r * cosf(angle), r * sinf(angle), -l/2);
        vertexes.push_back(v);
    }
    
    int base = 2 * (Divs + 1);
    
    for (int i = 0; i < 2 * Divs; i++) {
        Triangle t;
        t.indices.a = base + i;
        t.indices.b = base + i + 1;
        t.indices.c = base + (i + 3) % (2 * Divs);
        
        triangles.push_back(t);

        t.indices.a = base + i;
        t.indices.b = base + (i + 3) % (2 * Divs);
        t.indices.c = base + (i + 2) % (2 * Divs);
        
        triangles.push_back(t);
    }
    
    
}