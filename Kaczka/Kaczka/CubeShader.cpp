//
//  CubeShader.cpp
//  Kaczka
//
//  Created by Adrian Krupa on 26.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "CubeShader.h"


std::string Shader::CubeShader::getShaderName()
{
    return "CubeShader";
}

void Shader::CubeShader::bindAttributeLocations()
{
    glBindAttribLocation(m_program, VertexAttribPosition, "in_Position");
    glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal");
    
}

void Shader::CubeShader::fetchUniformLocations()
{
    m_viewProjectionMatrixUniform = glGetUniformLocation(m_program, "viewProjectionMatrix");
    m_modelMatrixUniform = glGetUniformLocation(m_program, "modelMatrix");
    
    m_cubeMapUniform = glGetUniformLocation(m_program, "cubeTex");
}

GLuint Shader::CubeShader::getViewProjectionMatrixUniform()
{
    return m_viewProjectionMatrixUniform;
}

GLuint Shader::CubeShader::getModelMatrixUniform()
{
    return m_modelMatrixUniform;
}

GLuint Shader::CubeShader::getCubeMapUniform()
{
    return m_cubeMapUniform;
}

GLuint Shader::CubeShader::getCameraPositionUniform()
{
    return m_cameraPositionUniform;
}
