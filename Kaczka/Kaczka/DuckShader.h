//
//  DuckShader.h
//  Kaczka
//
//  Created by Adrian Krupa on 27.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Kaczka__DuckShader__
#define __Kaczka__DuckShader__

#include <iostream>
#include "Shader.h"

namespace Shader {
    class DuckShader : public Shader {
        
    private:
        GLuint m_viewProjectionMatrixUniform;
        GLuint m_modelMatrixUniform;
        GLuint m_textureUniform;
        
        virtual std::string getShaderName();
        
        virtual void bindAttributeLocations();
        virtual void fetchUniformLocations();
    public:
        GLuint getViewProjectionMatrixUniform();
        GLuint getModelMatrixUniform();
        GLuint getTextureUniform();
    };
}
#endif /* defined(__Kaczka__DuckShader__) */
