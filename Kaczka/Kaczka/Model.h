//
//  Model.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 01/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__Model__
#define __Robot__Model__

#include <iostream>

#include "glm/glm.hpp"

#include "Mesh.h"

namespace Model {
    class Model {
        glm::mat4 m_transformMatrix;
        Mesh *m_mesh;
    public:
        glm::vec4 color;

        Model(Mesh *mesh);
        
        Mesh *getMesh();
        glm::mat4 getTransformMatrix();
        void setTransformMatrix(glm::mat4 matrix);
    };
}

#endif /* defined(__Robot__Model__) */
