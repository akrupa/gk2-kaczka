//
//  BoxMesh.cpp
//  Kaczka
//
//  Created by Adrian Krupa on 26.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "BoxMesh.h"

BoxMesh::BoxMesh() : Mesh() {
    Vertex v;
    Triangle t;

    v.normal = glm::vec3(0.0, 0.0, 1.0);
    
    v.position = glm::vec3(-1.0f, 1.0f, -1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f, 1.0f, -1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f,-1.0f, -1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-1.0f,-1.0f, -1.0f);
    vertexes.push_back(v);
    
    t.indices.a = 2;
    t.indices.b = 1;
    t.indices.c = 0;
    
    triangles.push_back(t);
    
    t.indices.a = 3;
    t.indices.b = 2;
    t.indices.c = 0;
    
    triangles.push_back(t);
    
    
    v.normal = glm::vec3(0.0, 0.0, -1.0);
    
    v.position = glm::vec3(-1.0f, 1.0f, 1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f, 1.0f, 1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f,-1.0f, 1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-1.0f,-1.0f, 1.0f);
    vertexes.push_back(v);
    
    t.indices.a = 4;
    t.indices.b = 5;
    t.indices.c = 6;
    
    triangles.push_back(t);
    
    t.indices.a = 4;
    t.indices.b = 6;
    t.indices.c = 7;
    
    triangles.push_back(t);
    
    t.indices.a = 3;
    t.indices.b = 0;
    t.indices.c = 4;
    triangles.push_back(t);
    
    t.indices.a = 3;
    t.indices.b = 4;
    t.indices.c = 7;
    triangles.push_back(t);
    
    t.indices.a = 6;
    t.indices.b = 5;
    t.indices.c = 1;
    triangles.push_back(t);
    
    t.indices.a = 6;
    t.indices.b = 1;
    t.indices.c = 2;
    triangles.push_back(t);
    
    t.indices.a = 1;
    t.indices.b = 5;
    t.indices.c = 4;
    triangles.push_back(t);
    
    t.indices.a = 1;
    t.indices.b = 4;
    t.indices.c = 0;
    triangles.push_back(t);
    
    t.indices.a = 6;
    t.indices.b = 2;
    t.indices.c = 3;
    triangles.push_back(t);
    
    t.indices.a = 6;
    t.indices.b = 3;
    t.indices.c = 7;
    triangles.push_back(t);

}
