//
//  PlaneMesh.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 06/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__PlaneMesh__
#define __Robot__PlaneMesh__

#include <iostream>
#include "Mesh.h"

class PlaneMesh : public Mesh {

public:
    PlaneMesh();
};

#endif /* defined(__Robot__PlaneMesh__) */
