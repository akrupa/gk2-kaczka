//
//  PlaneMesh.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 06/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "PlaneMesh.h"


PlaneMesh::PlaneMesh() : Mesh()
{
    Vertex v;
    v.normal = glm::vec3(0.0f, 0.0f, 1.0f);
    
    v.position = glm::vec3(-1.0f, 1.0f, 0.0f);
    vertexes.push_back(v);

    v.position = glm::vec3( 1.0f, 1.0f, 0.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f,-1.0f, 0.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-1.0f,-1.0f, 0.0f);
    vertexes.push_back(v);
    
    Triangle t;
    t.indices.a = 0;
    t.indices.b = 1;
    t.indices.c = 2;
    
    triangles.push_back(t);
    
    t.indices.a = 0;
    t.indices.b = 2;
    t.indices.c = 3;
    
    triangles.push_back(t);
}