#version 400

in vec3 pass_normal;
in vec2 pass_TexCoord;

out vec4 out_Color;

uniform sampler2D tex;

void main(void) {
    
    out_Color = texture(tex,pass_TexCoord);
}