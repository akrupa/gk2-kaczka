//
//  BoxMesh.h
//  Kaczka
//
//  Created by Adrian Krupa on 26.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Kaczka__BoxMesh__
#define __Kaczka__BoxMesh__

#include <iostream>
#include "Mesh.h"

class BoxMesh : public Mesh {
    
public:
    BoxMesh();
};

#endif /* defined(__Kaczka__BoxMesh__) */
