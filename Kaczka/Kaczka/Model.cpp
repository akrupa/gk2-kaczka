//
//  Model.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 01/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "Model.h"

Model::Model::Model(Mesh *mesh) : m_mesh(mesh), m_transformMatrix(glm::mat4())
{
    
}



glm::mat4 Model::Model::getTransformMatrix()
{
    return m_transformMatrix;
}

void Model::Model::setTransformMatrix(glm::mat4 matrix)
{
    m_transformMatrix = matrix;
}

Mesh * Model::Model::getMesh()
{
    return m_mesh;
}