#version 400

uniform mat4 viewProjectionMatrix;
uniform mat4 modelMatrix;


in vec3 in_Position;
in vec3 in_Normal;
in vec2 in_texcoord;

out vec2 pass_texcoord;
out vec3 pass_normal;
out vec4 pass_position;

out vec3 pass_tex_pos;

void main(void)
{
    vec4 worldPosition = modelMatrix * vec4(in_Position, 1.0);

	pass_normal = in_Normal;
    pass_position = worldPosition;
    pass_tex_pos = vec3(in_texcoord.x, 1-in_texcoord.y, 1.0);
    
    pass_texcoord = vec2(in_texcoord.x, 1-in_texcoord.y);
    
	gl_Position = viewProjectionMatrix * worldPosition;
}