//
//  CubeShader.h
//  Kaczka
//
//  Created by Adrian Krupa on 26.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Kaczka__CubeShader__
#define __Kaczka__CubeShader__

#include <iostream>
#include "Shader.h"

namespace Shader {
    class CubeShader : public Shader {
        
    private:
        GLuint m_viewProjectionMatrixUniform;
        GLuint m_modelMatrixUniform;
        
        GLuint m_cubeMapUniform;
        GLuint m_cameraPositionUniform;
        
        virtual std::string getShaderName();
        
        virtual void bindAttributeLocations();
        virtual void fetchUniformLocations();
    public:
        GLuint getViewProjectionMatrixUniform();
        GLuint getModelMatrixUniform();
        
        GLuint getCubeMapUniform();
        GLuint getCameraPositionUniform();
    };
}


#endif /* defined(__Kaczka__CubeShader__) */
