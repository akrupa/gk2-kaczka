//
//  Shader.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 16/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__Shader__
#define __Robot__Shader__

#include <iostream>

#include <OpenGL/gl3.h>

typedef enum : GLuint {
    VertexAttribPosition,
    VertexAttribNormal,
    VertexAttribAngle,
    VertexAttribAlpha,
    VertexAttribTexCoord
} VertexAttrib;

namespace Shader {
    class Shader {
        
    protected:
        GLuint m_program;
        
    protected:
        bool compileShader(GLuint *shader, GLenum type, std::string filePath);
        bool linkProgram(GLuint prog);
        
    public:
        Shader();
        ~Shader();
        
        GLuint getProgram();
        
        virtual void setup();
        
        virtual std::string getShaderName();
        
        virtual void bindAttributeLocations();
        virtual void fetchUniformLocations();
        

    };
}
#endif /* defined(__Robot__Shader__) */
