#version 400

uniform mat4 viewProjectionMatrix;
uniform mat4 modelMatrix;

in vec3 in_Position;
in vec3 in_Normal;

out vec3 pass_normal;
out vec3 pass_position;

void main(void)
{
    vec4 worldPosition = modelMatrix * vec4(in_Position, 1.0);
    
	pass_normal = in_Normal;
    pass_position = worldPosition.xyz;
    
	gl_Position = viewProjectionMatrix * worldPosition;
}