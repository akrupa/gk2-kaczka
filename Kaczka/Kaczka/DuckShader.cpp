//
//  DuckShader.cpp
//  Kaczka
//
//  Created by Adrian Krupa on 27.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "DuckShader.h"

std::string Shader::DuckShader::getShaderName()
{
    return "DuckShader";
}

void Shader::DuckShader::bindAttributeLocations()
{
    glBindAttribLocation(m_program, VertexAttribPosition, "in_Position");
    glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal");
    glBindAttribLocation(m_program, VertexAttribTexCoord, "in_TexCoord");
}

void Shader::DuckShader::fetchUniformLocations()
{
    m_viewProjectionMatrixUniform = glGetUniformLocation(m_program, "viewProjectionMatrix");
    m_modelMatrixUniform = glGetUniformLocation(m_program, "modelMatrix");
    m_textureUniform = glGetUniformLocation(m_program, "tex");
}

GLuint Shader::DuckShader::getViewProjectionMatrixUniform()
{
    return m_viewProjectionMatrixUniform;
}

GLuint Shader::DuckShader::getModelMatrixUniform()
{
    return m_modelMatrixUniform;
}

GLuint Shader::DuckShader::getTextureUniform()
{
    return m_textureUniform;
}
