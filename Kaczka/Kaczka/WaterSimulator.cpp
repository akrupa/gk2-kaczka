//
//  WaterSimulator.cpp
//  Kaczka
//
//  Created by Adrian Krupa on 25.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "WaterSimulator.h"
#include "SOIL/SOIL.h"
#include "glm/glm.hpp"
#include <algorithm>
#include <thread>

Simulator::WaterSimulator::WaterSimulator() {
    
    bool loadNormalFromMap = false;
    N = 256;
    
    
    if (loadNormalFromMap) {
        normalMap = SOIL_load_image("normal_3.jpg", &N, &N, NULL, 0);
    } else {
        normalMap = new unsigned char[N*N*3];
        for (int i=0; i<N*N*3; i+=3) {
            normalMap[i]=0;
            normalMap[i+1]=0;
            normalMap[i+2] = 255;
        }
        
    }
    
    h = 2.0f/(N-1);
    c = 1;
    delta_t = 1.0f/N;
    
    d = new float[N*N];
    
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            float bok = 2;
            float minX = std::min(N-i-1, i);
            float minY = std::min(N-j-1, j);
            float l = (std::min(minX, minY)/128.0)*bok;
            d[i*N+j] = 0.95 * std::min(1.0f, l/0.2f);
        }
    }
    
    height = new float[N*N];
    prev_height = new float[N*N];
    
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            height[i*N+j] = 0;
            prev_height[i*N+j] = 0;
        }
    }
    


    
    glGenTextures(1, &cubeTex);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTex);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 0);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    
    
    
    unsigned char* cubeMap;
    int cube_width;
    int cube_height;
    
    cubeMap = SOIL_load_image("cube5.jpg", &cube_width, &cube_height, NULL, 0);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, cube_width, cube_height, 0, GL_RGB, GL_UNSIGNED_BYTE, cubeMap);
    SOIL_free_image_data(cubeMap);
    
    cubeMap = SOIL_load_image("cube3.jpg", &cube_width, &cube_height, NULL, 0);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, cube_width, cube_height, 0, GL_RGB, GL_UNSIGNED_BYTE, cubeMap);
    SOIL_free_image_data(cubeMap);
    
    cubeMap = SOIL_load_image("cube1.jpg", &cube_width, &cube_height, NULL, 0);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, cube_width, cube_height, 0, GL_RGB, GL_UNSIGNED_BYTE, cubeMap);
    SOIL_free_image_data(cubeMap);
    
    cubeMap = SOIL_load_image("cube4.jpg", &cube_width, &cube_height, NULL, 0);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, cube_width, cube_height, 0, GL_RGB, GL_UNSIGNED_BYTE, cubeMap);
    SOIL_free_image_data(cubeMap);
    
    cubeMap = SOIL_load_image("cube2.jpg", &cube_width, &cube_height, NULL, 0);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, cube_width, cube_height, 0, GL_RGB, GL_UNSIGNED_BYTE, cubeMap);
    SOIL_free_image_data(cubeMap);
    
    cubeMap = SOIL_load_image("cube6.jpg", &cube_width, &cube_height, NULL, 0);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, cube_width, cube_height, 0, GL_RGB, GL_UNSIGNED_BYTE, cubeMap);
    SOIL_free_image_data(cubeMap);
    
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, N, N, 0, GL_RGB, GL_UNSIGNED_BYTE, normalMap);
    
    int w,h;
    GLuint tt;
    unsigned char* duckTexture = SOIL_load_image("ducktex.jpg", &w, &h, NULL, 0);
    glGenTextures(1, &tt);
    glBindTexture(GL_TEXTURE_2D, tt);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, duckTexture);
    
    glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, tt);
    glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, tex);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTex);
    
}



Simulator::WaterSimulator::~WaterSimulator() {
    free(normalMap);
    free(d);
    free(height);
    free(prev_height);
}

void Simulator::WaterSimulator::UpdateWater() {
    
    float r = rand()%10;
    if (r == 0) {
        height[(rand()%N)*N + rand()%N]=0.01;
        prev_height[(rand()%N)*N + rand()%N]=0.01;
    }
    
    float A = (c*c*delta_t*delta_t)/(h*h);
    float B = 2-4*A;
    
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            float sum = 0;
            if (i<N-1) {
                sum += prev_height[(i+1)*N+j];
            }
            if (i>0) {
                sum += prev_height[(i-1)*N+j];
            }
            if (j<N-1) {
                sum += prev_height[i*N+j+1];
            }
            if (j>0) {
                sum += prev_height[i*N+j-1];
            }
            height[i*N+j] = d[i*N+j] * (A*sum + B*prev_height[i*N+j] - height[i*N+j]);
        }
    }
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            float leftX = j - 1 < 0 ? 0 : height[i*N + j - 1];
            float rightX = j + 1 >= N ? 0 : height[i*N+j + 1];
            float leftY = i - 1 < 0 ? 0 : height[(i - 1)*N+j];
            float rightY = i + 1 >= N ? 0 : height[(i + 1)*N+j];
            glm::vec3 normal = glm::vec3(2 * h*(leftX - rightX), 4 * h*h, 2 * h*(leftY - rightY));
            normal = glm::normalize(normal);
            
            normalMap[i*N*3+j*3+0] = normal[0]*255;
            normalMap[i*N*3+j*3+1] = normal[1]*255;
            normalMap[i*N*3+j*3+2] = normal[2]*255;
        }
    }
    /*
    int threadsCount = 16;
    
    for (int i=0; i<threadsCount; i++) {
        startThread((i*N)/threadsCount, ((i+1)*N)/threadsCount);
    }
    
    for (int i=0; i<threads.size(); i++) {
        threads[i].join();
    }
    threads.clear();
    */
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, N, N, 0, GL_RGB, GL_UNSIGNED_BYTE, normalMap);
    
    float *tmp = prev_height;
    prev_height = height;
    height = tmp;
}

void Simulator::WaterSimulator::ThreadTask(int from, int to) {
    
    
    for (int i=from; i<to; i++) {
        for (int j=0; j<N; j++) {
            glm::vec3 normal;
            if (i>0 && j>0) {
                glm::vec3 v0(-1, 1, height[(i-1)*N+(j-1)]);
                glm::vec3 v1(-1, 0, height[(i-1)*N+j]);
                glm::vec3 v2(0, 0, height[i*N+j]);
                
                glm::vec3 side1 = v1-v0;
                glm::vec3 side2 = v2-v1;
                glm::vec3 norm = glm::cross(side1, side2);
                normal += norm;
                
                glm::vec3 v3(0, 1, height[i*N+(j-1)]);
                
                glm::vec3 side4 = v2-v0;
                glm::vec3 side5 = v3-v2;
                glm::vec3 norm2 = glm::cross(side4, side5);
                normal += norm2;
            }
            
            if (i<N-1 && j>0) {
                glm::vec3 v0(0, 1, height[i*N+(j-1)]);
                glm::vec3 v1(0, 0, height[i*N+j]);
                glm::vec3 v2(1, 0, height[(i+1)*N+j]);
                
                glm::vec3 side1 = v1-v0;
                glm::vec3 side2 = v2-v1;
                glm::vec3 norm = glm::cross(side1, side2);
                normal += norm;
            }
            
            if (i>0 && j<N-1) {
                glm::vec3 v0(0, 0, height[i*N+j]);
                glm::vec3 v1(-1, 0, height[(i-1)*N+j]);
                glm::vec3 v2(0, -1, height[i*N+(j+1)]);
                
                glm::vec3 side1 = v1-v0;
                glm::vec3 side2 = v2-v1;
                glm::vec3 norm = glm::cross(side1, side2);
                normal += norm;
            }
            if (i<N-1 && j<N-1) {
                glm::vec3 v0(0, 0, height[i*N+j]);
                glm::vec3 v1(0, -1, height[i*N+(j+1)]);
                glm::vec3 v2(1, -1, height[(i+1)*N+(j+1)]);
                
                glm::vec3 side1 = v1-v0;
                glm::vec3 side2 = v2-v1;
                glm::vec3 norm = glm::cross(side1, side2);
                normal += norm;
                
                glm::vec3 v3(1, 0, height[(i+1)*N+j]);
                
                glm::vec3 side4 = v2-v0;
                glm::vec3 side5 = v3-v2;
                glm::vec3 norm2 = glm::cross(side4, side5);
                normal += norm2;
            }
            
            if (normal[0]==0 && normal[1] == 0 && normal[2] == 0) {
                normal[2] = 1;
            }
            normal = glm::normalize(normal);
            
            normalMap[i*N*3+j*3+0] = normal[0]*255;
            normalMap[i*N*3+j*3+1] = normal[1]*255;
            normalMap[i*N*3+j*3+2] = normal[2]*255;
        }
    }
}

GLuint Simulator::WaterSimulator::texId()
{
    return tex;
}

GLuint Simulator::WaterSimulator::cubeTexId()
{
    return cubeTex;
}

