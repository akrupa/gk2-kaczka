//
//  WaterSimulator.h
//  Kaczka
//
//  Created by Adrian Krupa on 25.05.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Kaczka__WaterSimulator__
#define __Kaczka__WaterSimulator__

#include <iostream>
#include <OpenGL/gl3.h>
#include <thread>
#include <vector>

namespace Simulator {
    class WaterSimulator {
        
        unsigned char* normalMap;
        float *d;
        float *height;
        float *prev_height;
        GLuint tex;
        GLuint cubeTex;
        
        int N;
        float c;
        float delta_t;
        float h;
        
        std::vector<std::thread>threads;

        void ThreadTask(int from, int to);
        void startThread(int from, int to) {
            threads.push_back(std::thread(&WaterSimulator::ThreadTask, this, from, to));
        }
    public:
        WaterSimulator();
        ~WaterSimulator();
        void UpdateWater();
        
        GLuint texId();
        GLuint cubeTexId();
    };
}

#endif /* defined(__Kaczka__WaterSimulator__) */
