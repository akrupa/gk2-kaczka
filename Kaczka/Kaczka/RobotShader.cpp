//
//  RobotShader.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 23/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "RobotShader.h"



std::string Shader::RobotShader::getShaderName()
{
    return "RobotShader";
}

void Shader::RobotShader::bindAttributeLocations()
{
    glBindAttribLocation(m_program, VertexAttribPosition, "in_Position");
    glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal");
    glBindAttribLocation(m_program, VertexAttribTexCoord, "in_texcoord");
    
}

void Shader::RobotShader::fetchUniformLocations()
{
    m_viewProjectionMatrixUniform = glGetUniformLocation(m_program, "viewProjectionMatrix");
    m_modelMatrixUniform = glGetUniformLocation(m_program, "modelMatrix");

    m_lightPositionUniform = glGetUniformLocation(m_program, "lightPosition");
    m_normalMapUniform = glGetUniformLocation(m_program, "tex");
    m_cubeMapUniform = glGetUniformLocation(m_program, "cubeTex");
    m_cameraPositionUniform = glGetUniformLocation(m_program, "cameraPosition");
}



GLuint Shader::RobotShader::getViewProjectionMatrixUniform()
{
    return m_viewProjectionMatrixUniform;
}

GLuint Shader::RobotShader::getLightPositionUniform()
{
    return m_lightPositionUniform;
}


GLuint Shader::RobotShader::getModelMatrixUniform()
{
    return m_modelMatrixUniform;
}

GLuint Shader::RobotShader::getCubeMapUniform()
{
    return m_cubeMapUniform;
}

GLuint Shader::RobotShader::getNormalMapUniform()
{
    return m_normalMapUniform;
}

GLuint Shader::RobotShader::getCameraPositionUniform()
{
    return m_cameraPositionUniform;
}
