#version 400

in vec2 pass_texcoord;
in vec3 pass_normal;
in vec4 pass_position;
in vec3 pass_tex_pos;

out vec4 out_Color;

uniform samplerCube cubeTex;
uniform sampler2D tex;
uniform vec3 lightPosition;
uniform vec3 cameraPosition;

vec3 ray_to_texcoord(vec3 pointer, vec3 dir)
{
    vec3 p = pointer + 3.0 * dir;
    float m = max(abs(p.x), max(abs(p.y), abs(p.z)));
    p/=m;
    return p;
}

vec4 reflectedColor(vec3 pos, vec3 cameraPos, vec3 normal)
{
    vec3 direction = pos.xyz-cameraPos;
    vec3 reflectedDirection = reflect(direction, normal);

    vec3 boxMax = vec3(2,2,2);
    vec3 boxMin = vec3(-2,-2,-2);
    
    
    vec3 FirstPlaneIntersect = (boxMax - pos) / reflectedDirection;
    vec3 SecondPlaneIntersect = (boxMin - pos) / reflectedDirection;
    
    vec3 FurthestPlane = max(FirstPlaneIntersect, SecondPlaneIntersect);
    float Distance = min(min(FurthestPlane.x, FurthestPlane.y), FurthestPlane.z);
    
    vec3 IntersectPosition = pos + reflectedDirection * Distance;
    reflectedDirection = IntersectPosition;
    
    return texture(cubeTex, reflectedDirection);
}

vec4 refractedColor(vec3 pos, vec3 cameraPos, vec3 normal)
{
    vec3 direction = pos-cameraPos;
    vec3 reflectedDirection = refract(direction, normal, 0.66);
    vec3 boxMax = vec3(2,2,2);
    vec3 boxMin = vec3(-2,-2,-2);
    
    
    vec3 FirstPlaneIntersect = (boxMax - pos) / reflectedDirection;
    vec3 SecondPlaneIntersect = (boxMin - pos) / reflectedDirection;
    
    vec3 FurthestPlane = max(FirstPlaneIntersect, SecondPlaneIntersect);
    float Distance = min(min(FurthestPlane.x, FurthestPlane.y), FurthestPlane.z);
    
    vec3 IntersectPosition = pos + reflectedDirection * Distance;
    reflectedDirection = IntersectPosition;
    
    return texture(cubeTex, reflectedDirection);
}

void main(void) {
    
    vec4 worldPosition = pass_position;
    vec4 tex_normal = texture(tex, pass_texcoord).xzyw;
    
    vec4 reflection = reflectedColor(pass_position.xyz, -cameraPosition, tex_normal.xzy);
    vec4 refraction = refractedColor(pass_position.xyz, -cameraPosition, tex_normal.xzy);
     vec4 normal = normalize(tex_normal.xzyw);
    
    vec4 toLight = vec4(lightPosition,1.0) - worldPosition;
    toLight = normalize(toLight);
	float intensity = max(dot(normal, toLight), 0);
    
    vec4 color = vec4(0.1, 0.4, 0.9, 1)*reflection;
    out_Color = color * vec4(intensity, intensity, intensity, 0.5);
    
    vec3 direction = normalize(pass_position.xyz+cameraPosition);
    vec4 reflrefr;
    if (direction.y<0) {
        reflrefr = mix(reflection, refraction, min(abs(direction.y)*1.5,1));
    } else {
        reflrefr = mix(refraction, reflection, min(abs(direction.y)*1.5,1));
    }
    vec4 outputColot = mix(reflrefr, vec4(0.1, 0.4, 0.9, 1), 0.05);
    out_Color = outputColot * vec4(intensity, intensity, intensity, 1);
    out_Color = outputColot;
}