//
//  Buffer.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 16/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__Buffer__
#define __Robot__Buffer__

#include <iostream>
#include <OpenGL/gl3.h>



namespace Buffer {
    class Buffer {
    protected:
        GLuint m_VAO;
        GLuint m_indiciesCount;
        
        GLuint m_indexBuffer;
        GLuint m_vertexBuffer;
        GLuint m_texcoordsBuffer;
        
    public:
        GLuint getVAO();
        GLuint getIndicesCount();
    };
}

#endif /* defined(__Robot__Buffer__) */
