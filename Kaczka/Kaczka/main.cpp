//
//  main.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 16/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include <iostream>
#include <GLUT/GLUT.h>
//#include <OpenGL/OpenGL.h>

#include "CameraController.h"
#include "Renderer.h"
#include "PlaneMesh.h"
#include "BoxMesh.h"
#include "CylinderMesh.h"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"

static const glm::vec3 WaterPosition = glm::vec3(0.0f, -1.0f, 0.0f);
static const glm::vec3 DuckPosition = glm::vec3(1.0f, -1.0f, 0.5f);
static const float WaterTiltAngle = M_PI_2;


static Controller::CameraController cameraController;
static Renderer::Renderer renderer;



// ugly, but whatever...
static int mousePrevX;
static int mousePrevY;


static int keyboardX;
static int keyboardY;
static int keyboardZ;

void moveHandler(int x, int y)
{
    mousePrevX = x;
    mousePrevY = y;
}

void dragHandler(int x, int y)
{
    int dx = x - mousePrevX;
    int dy = y - mousePrevY;
    
    cameraController.mouseDrag(dx, dy);
    
    mousePrevX = x;
    mousePrevY = y;
}


void keyboardHandler(unsigned char key, int x, int y)
{
    if (key == 'a') {
        keyboardX = +1;
    }
    if (key == 'd') {
        keyboardX = -1;
    }
    if (key == 'w') {
        keyboardZ = +1;
    }
    if (key == 's') {
        keyboardZ = -1;
    }
    if (key == 'q') {
        keyboardY = +1;
    }
    if (key == 'e') {
        keyboardY = -1;
    }
}

void keyboardUpHandler(unsigned char key, int x, int y)
{
    keyboardX = 0;
    keyboardY = 0;
    keyboardZ = 0;
}



void display(void)
{
    cameraController.keyboardMove(keyboardX, keyboardY, keyboardZ);
    
    renderer.renderWithCameraController(&cameraController);
    
    glutPostRedisplay();
}


Model::Model * mirrorMeshModel()
{
    PlaneMesh *mirrorMesh = new PlaneMesh();
    Model::Model * mirrorModel = new Model::Model(mirrorMesh);
    
    glm::mat4 rotation = glm::rotate(glm::mat4(), (float)-M_PI_2, glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 tilt = glm::rotate(glm::mat4(), WaterTiltAngle, glm::vec3(0.0f, 0.0f, 1.0f));
    glm::mat4 translation = glm::translate(glm::mat4(), WaterPosition);
    glm::mat4 scale = glm::scale(glm::mat4(), glm::vec3(2,2,2));
    
    mirrorModel->setTransformMatrix(translation * tilt * rotation * scale);
    mirrorModel->color = glm::vec4(0.5f, 0.5f, 0.5f, 0.5);
    
    return mirrorModel;
}

Model::Model *cubeMeshModel()
{
    BoxMesh *cubeMesh = new BoxMesh();
    Model::Model *cubeModel = new Model::Model(cubeMesh);
    
    glm::mat4 rotation = glm::rotate(glm::mat4(), 0.0f, glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 tilt = glm::rotate(glm::mat4(), 0.0f, glm::vec3(0.0f, 0.0f, 1.0f));
    glm::mat4 translation = glm::translate(glm::mat4(), WaterPosition);
    glm::mat4 scale = glm::scale(glm::mat4(), glm::vec3(2,2,2));
    
    cubeModel->setTransformMatrix(translation * tilt * rotation * scale);
    
    return cubeModel;
}

Model::Model *duckMeshModel()
{
    Mesh *duckMesh = new Mesh("duck.txt");
    Model::Model *duckModel = new Model::Model(duckMesh);
    
    glm::mat4 rotation = glm::rotate(glm::mat4(), 0.0f, glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 tilt = glm::rotate(glm::mat4(), 0.0f, glm::vec3(0.0f, 0.0f, 1.0f));
    glm::mat4 translation = glm::translate(glm::mat4(), DuckPosition);
    glm::mat4 scale = glm::scale(glm::mat4(), glm::vec3(0.005,0.005,0.005));
    
    duckModel -> setTransformMatrix(translation * tilt * rotation * scale);
    return duckModel;
}



void setup()
{
    renderer.setup();
    //renderer.setRobotModels(robotController.getArms());
    //renderer.setSceneModels(sceneModels());
    renderer.setMirrorModel(mirrorMeshModel());
    renderer.setCubeModel(cubeMeshModel());
    renderer.setDuckModel(duckMeshModel());
}

int main (int argc, char **argv) {
    glutInit(&argc,argv);
    
#ifdef __APPLE__
	glutInitDisplayMode( GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH);
#else
	// If you are using freeglut, the next two lines will check if
	// the code is truly 3.2. Otherwise, comment them out
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
    
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100,100);
    glutCreateWindow("Robot");
    
    glutDisplayFunc(display);
    glutPassiveMotionFunc(moveHandler);
    glutMotionFunc(dragHandler);
    glutKeyboardFunc(keyboardHandler);
    glutKeyboardUpFunc(keyboardUpHandler);
    
    setup();
    
    glutMainLoop();
    
    return 0;
}

