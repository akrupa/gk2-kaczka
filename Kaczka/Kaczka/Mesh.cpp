//
//  Mesh.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 01/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "Mesh.h"

#include <fstream>

using namespace std;
using namespace glm;

Mesh::Mesh()
{
    vertexes = vector<Vertex>();
    triangles = vector<Triangle>();
}

Mesh::Mesh(std::string fileName) : Mesh()
{

    std::ifstream file(fileName);

    
    int verticiesCount;
    file >> verticiesCount;

    for (int i = 0; i < verticiesCount; i++) {
        Vertex v;
        file >> v.position.x >> v.position.y >> v.position.z;
        file >> v.normal.x >> v.normal.y >> v.normal.z;
        file >> v.texCoords.x >> v.texCoords.y;
        vertexes.push_back(v);
    }
    
    
    int trianglesCount;
    file >> trianglesCount;
    
    for (int i = 0; i < trianglesCount; i++) {
        Triangle t;
        file  >> t.indices.v[0] >> t.indices.v[1] >> t.indices.v[2];
        triangles.push_back(t);
    }
}