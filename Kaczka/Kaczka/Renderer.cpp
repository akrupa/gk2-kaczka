//
//  Renderer.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 23/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "Renderer.h"
#include "Mesh.h"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"


using namespace glm;

void Renderer::Renderer::setup()
{
    glClearColor(0.2, 0.2, 0.2, 1.0);
    //glEnable(GL_DEPTH_CLAMP);
    m_lightPosition = vec3(-1.0f, 1.0f, -1.0f);
    
    robotShader = new Shader::RobotShader();
    robotShader->setup();
    
    cubeShader = new Shader::CubeShader();
    cubeShader -> setup();
    
    duckShader = new Shader::DuckShader();
    duckShader -> setup();
    
    m_waterSimulator = new Simulator::WaterSimulator();
}

void Renderer::Renderer::setMirrorModel(Model::Model *mirrorModel)
{
    m_mirrorModel = mirrorModel;
    
    m_mirrorBuffer = new Buffer::ArmMeshBuffer();
    m_mirrorBuffer->setup(false);
    
    m_mirrorBuffer->fillVertexData(mirrorModel->getMesh()->vertexes.data(),
                                   mirrorModel->getMesh()->vertexes.size() * sizeof(Vertex),
                                   0);
    
    m_mirrorBuffer->fillIndexData((unsigned int *)mirrorModel->getMesh()->triangles.data(),
                                  mirrorModel->getMesh()->triangles.size() * sizeof(Triangle),
                                  0);
}

void Renderer::Renderer::setCubeModel(Model::Model *cubeModel)
{
    m_cubeModel = cubeModel;
    
    m_cubeBuffer = new Buffer::ArmMeshBuffer();
    m_cubeBuffer->setup(false);
    
    m_cubeBuffer->fillVertexData(cubeModel->getMesh()->vertexes.data(),
                                 cubeModel->getMesh()->vertexes.size() * sizeof(Vertex),
                                 0);
    
    m_cubeBuffer->fillIndexData((unsigned int*)cubeModel->getMesh()->triangles.data(),
                                cubeModel->getMesh()->triangles.size() * sizeof(Triangle),
                                0);
}

void Renderer::Renderer::setDuckModel(Model::Model *duckModel)
{
    m_duckModel = duckModel;
    
    m_duckBuffer = new Buffer::ArmMeshBuffer();
    m_duckBuffer->setup(true);
    
    m_duckBuffer->fillVertexData(duckModel->getMesh()->vertexes.data(),
                                 duckModel->getMesh()->vertexes.size() * sizeof(Vertex),
                                 0);
    
    m_duckBuffer->fillIndexData((unsigned int*)duckModel->getMesh()->triangles.data(),
                                duckModel->getMesh()->triangles.size() * sizeof(Triangle),
                                0);
}



void Renderer::Renderer::renderWithCameraController(Controller::CameraController *camera)
{
    m_waterSimulator -> UpdateWater();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    
    drawCube(camera->getViewMatrix(), camera->getProjectionMatrix(), camera->getPosition());
    drawMirror(camera->getViewMatrix(), camera->getProjectionMatrix(), camera->getPosition());
    drawDuck(camera->getViewMatrix(), camera->getProjectionMatrix(), camera->getPosition());
    
    glFlush();
}

void Renderer::Renderer::drawDuck(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 cameraPosition)
{
    glEnable(GL_CULL_FACE);
    glUseProgram(duckShader->getProgram());
    glUniformMatrix4fv(duckShader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
    glUniform1i(duckShader->getTextureUniform(), 2);

    
    {
        glBindVertexArray(m_duckBuffer->getVAO());
        mat4 modelMatrix = m_duckModel->getTransformMatrix();
        
        GLuint indexCount = (GLuint)(m_duckModel->getMesh()->triangles.size() * 3);
        
        glUniformMatrix4fv(duckShader->getModelMatrixUniform(), 1, 0, glm::value_ptr(modelMatrix));
        
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }
    glUseProgram(0);
    glBindVertexArray(0);
}

void Renderer::Renderer::drawCube(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 cameraPosition)
{
    glEnable(GL_CULL_FACE);
    glUseProgram(cubeShader->getProgram());
    glUniform1i(cubeShader->getCubeMapUniform(), 0);
    glUniformMatrix4fv(cubeShader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
    
    glUniform3fv(cubeShader->getCameraPositionUniform(), 1, glm::value_ptr(cameraPosition));
    
    {
        glBindVertexArray(m_cubeBuffer->getVAO());
        mat4 modelMatrix = m_cubeModel->getTransformMatrix();
        
        GLuint indexCount = (GLuint)(m_cubeModel->getMesh()->triangles.size() * 3);
        
        glUniformMatrix4fv(cubeShader->getModelMatrixUniform(), 1, 0, glm::value_ptr(modelMatrix));
        
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }
    glUseProgram(0);
    glBindVertexArray(0);
}


void Renderer::Renderer::drawMirror(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 cameraPosition)
{
    glDisable(GL_CULL_FACE);
    
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glUseProgram(robotShader->getProgram());
    
    glUniform1i(robotShader->getCubeMapUniform(), 0);
    glUniform1i(robotShader->getNormalMapUniform(), 1);
    
    glUniformMatrix4fv(robotShader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
    
    glUniform3fv(robotShader->getLightPositionUniform(), 1, glm::value_ptr(m_lightPosition));
    glUniform3fv(robotShader->getCameraPositionUniform(), 1, glm::value_ptr(cameraPosition));
    
    {
        glBindVertexArray(m_mirrorBuffer->getVAO());
        mat4 modelMatrix = m_mirrorModel->getTransformMatrix();
        
        GLuint indexCount = (GLuint)(m_mirrorModel->getMesh()->triangles.size() * 3);
        
        glUniformMatrix4fv(robotShader->getModelMatrixUniform(), 1, 0, glm::value_ptr(modelMatrix));
        
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }
    
    glUseProgram(0);
    glBindVertexArray(0);
}