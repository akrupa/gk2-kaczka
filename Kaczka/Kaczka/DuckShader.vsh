#version 400

uniform mat4 viewProjectionMatrix;
uniform mat4 modelMatrix;

in vec3 in_Position;
in vec3 in_Normal;
in vec2 in_TexCoord;

out vec3 pass_normal;
out vec2 pass_TexCoord;

void main(void)
{
    vec4 worldPosition = modelMatrix * vec4(in_Position, 1.0);
    
	pass_normal = in_Normal;
    pass_TexCoord = in_TexCoord;
    
	gl_Position = viewProjectionMatrix * worldPosition;
}