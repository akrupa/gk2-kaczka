//
//  ArmMeshBuffer.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 16/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "ArmMeshBuffer.h"
#include "Shader.h"

static const GLuint VertexLimit = USHRT_MAX;
static const GLuint IndexLimit = VertexLimit * 6;



void Buffer::ArmMeshBuffer::setup(bool texture)
{
    glGenVertexArrays(1, &m_VAO);
    glBindVertexArray(m_VAO);
    
    
    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, VertexLimit * sizeof(Vertex), NULL, GL_STATIC_DRAW);
    
    glGenBuffers(1, &m_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, IndexLimit * sizeof(GLuint), NULL, GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(VertexAttribPosition);
 
    glVertexAttribPointer(VertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
    glEnableVertexAttribArray(VertexAttribNormal);

    glVertexAttribPointer(VertexAttribNormal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
    
    if (texture) {
        glEnableVertexAttribArray(VertexAttribTexCoord);
        glVertexAttribPointer(VertexAttribTexCoord, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));
        
    } else {
        GLfloat cube_texcoords[] = {
            // front
            0.0, 0.0,
            1.0, 0.0,
            1.0, 1.0,
            0.0, 1.0,
        };
        glGenBuffers(1, &m_texcoordsBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, m_texcoordsBuffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cube_texcoords), cube_texcoords, GL_STATIC_DRAW);
        
        glEnableVertexAttribArray(VertexAttribTexCoord);
        glBindBuffer(GL_ARRAY_BUFFER, m_texcoordsBuffer);
        glVertexAttribPointer(
                              VertexAttribTexCoord, // attribute
                              2,                  // number of elements per vertex, here (x,y)
                              GL_FLOAT,           // the type of each element
                              GL_FALSE,           // take our values as-is
                              0,                  // no extra data between each position
                              0                   // offset of first element
                              );
    }

    
    glBindVertexArray(0);
}


void Buffer::ArmMeshBuffer::fillVertexData(Vertex *data, size_t size, size_t offset)
{
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Buffer::ArmMeshBuffer::fillIndexData(GLuint *data, size_t size, size_t offset)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, size, data);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}